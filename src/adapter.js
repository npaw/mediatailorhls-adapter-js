var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.MediatailorHls = youbora.Adapter.extend({

  getVersion: function () {
    return manifest.version + '-MediatailorHls-ads'
  },

  getPlayhead: function () {
    return this.player.media.currentTime - this.currentAd.startTime
  },

  getTitle: function () {
    return this.currentAd.title
  },

  getDuration: function () {
    return this.currentAd.duration
  },

  getCreativeId: function () {
    return this.currentAd.creativeId
  },

  getProvider: function () {
    return this.currentAd.adSystem
  },

  getResource: function () {
    return this.urlComponents.url
  },

  getIsFullscreen: function () {
    var ret = false
    if (this.tag && typeof window !== 'undefined') {
      var cW = this.tag.clientWidth 
      var cH = this.tag.clientHeight
      var wH = window.innerHeight
      var wW = window.innerWidth
      if (cW && cH && wH && wW) {
        ret = (wH <= cH + 30 && wW <= cW + 30)
      }
    }
    return ret
  },

  getIsVisible: function () {
    return youbora.Util.calculateAdViewability(this.tag)
  },

  getAudioEnabled: function () {
    var ret = !this.tag.muted
    if (ret) {
      ret = this.tag.volume > 0
    }
    return ret
  },

  getIsSkippable: function () {
    return false
  },

  getPosition: function () {
    var plugin = this.plugin
    var constants = youbora.Constants.AdPosition
    var playhead = plugin ? plugin.getPlayhead() : 0
    var duration = plugin ? plugin.getDuration() : 0
    var isJoined = plugin && plugin.getAdapter() ? plugin.getAdapter().flags.isJoined : true
    var isLive = plugin ? plugin.getIsLive() : false
    var ret = constants.Midroll
    if (!isJoined || playhead < 1) {
      ret = constants.Preroll
    } else if (!isLive && playhead + 1 > duration) {
      ret = constants.Postroll
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    this.urlComponents = {}
    this.currentAd = {}
    this.monitorPlayhead(true, false)
    this.references = {
      hlsFragChanged: this.changedListener.bind(this),
      hlsMediaAttached: this.getTagListener.bind(this)
    }
    this.tagReferences = {
      pause: this.pauseListener.bind(this),
      playing: this.playingListener.bind(this),
      seeking: this.seekingListener.bind(this),
      seeked: this.seekedListener.bind(this),
      timeupdate: this.quartileListener.bind(this)
    }
    if (this.player) {
      for (var key in this.references) {
        this.player.on(key, this.references[key])
      }
    }
    this.getTagListener()
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()
    if (this.player) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
    }
    if (this.tag) {
      for (var key2 in this.tagReferences) {
        this.tag.removeEventListener(key2, this.tagReferences[key2])
      }
    }
  },

  /** Listener for 'hlsFragChanged' event. */
  changedListener: function (e, m) {
    // For submanifest with ads
    var childPlaylistUrl = m.frag.baseurl
    if (childPlaylistUrl.lastIndexOf('mediatailor') > 0){
      var subUrls = childPlaylistUrl.split('/')
      this.urlComponents = {
        url: childPlaylistUrl,
        protocol: subUrls[0],
        domain: subUrls[2],
        version: subUrls[3],
        accId: subUrls[5],
        originID: subUrls[6],
        session: subUrls[7]
      }
      this._requestData()
    }
    // For segment info
    var url = m.frag.relurl
    for (var tag in m.frag.tagList) {
      var firstTag = m.frag.tagList[tag][0]
      if (firstTag === 'DIS') {
        if (this.flags.isStarted) {
          this._stop()
        } else {
          this._start()
        }
      }
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function () {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function () {
    this.fireResume()
  },

  /** Listener for 'timeupdate' event. */
  quartileListener: function () {
    if (this.flags.isJoined) {
      var time = this.getPlayhead() / this.getDuration()
      if (time > 1) {
        this._stop()
      } else if (time > 0.75) {
        this.fireQuartile(3)
      } else if (time > 0.5) {
        this.fireQuartile(2)
      } else if (time > 0.25) {
        this.fireQuartile(1)
      }
    }
  },

  seekingListener: function () {
    this._stop()
  },

  seekedListener: function () {
    this._start()
  },

  /** Listener for 'hlsMediaAttached' event. */
  getTagListener: function () {
    this.tag = this.player ? this.player.media : null
    if (this.tag) {
      for (var key in this.tagReferences) {
        this.tag.addEventListener(key, this.tagReferences[key])
      }
    }
  },

  _requestData: function() {
    var comp = this.urlComponents
    var reqUrl = comp.protocol + '//' + comp.domain + '/' + comp.version + '/tracking/' + comp.accId + '/' + comp.originID + '/' + comp.session
    var req = new youbora.Request(reqUrl)
    req.on(youbora.Request.Event.SUCCESS, function(e) {
      this._parseApiResponse(JSON.parse(e.xhr.response))
    }.bind(this))
    req.send()
  },

  _parseApiResponse: function(resp) {
    try {
      if (!this.flags.isStarted && resp.avails.length) {
        this.ads = resp.avails
      }
    } catch (e) {
      youbora.Log.warn('Wrong response format: ', e)
    }
  },

  _start: function () {
    if (this.ads && this.ads.length > 0){
      for (i = (this.ads.length - 1); i >= 0; i--) {
        var adObj = this.ads[i].ads[0]
        if (this._isInAd({
          startTime: adObj.startTimeInSeconds ,
          endTime: adObj.startTimeInSeconds + adObj.durationInSeconds
        })) {
          this.currentAd = {
            duration: adObj.durationInSeconds,
            startTime: adObj.startTimeInSeconds,
            endTime: adObj.startTimeInSeconds + adObj.durationInSeconds,
            system: adObj.adSystem,
            creativeId: adObj.creativeId,
            title: adObj.adTitle,
            vastId: adObj.vastAdId
          }
          if (this.plugin && this.plugin.getAdapter()) {
            this.plugin.getAdapter().unregisterListeners()
            this.plugin.getAdapter().firePause()
          }
          this.fireStart()
          this.fireJoin()
          break
        }
      }
    }
  },

  _stop: function () {
    if (this.flags.isStarted) {
      if (!this._isInAd(this.currentAd)) {
        this.fireStop()
        this.fireBreakStop()
        if (this.plugin && this.plugin.getAdapter()) {
          this.plugin.getAdapter().fireResume()
          this.plugin.getAdapter().registerListeners()
        }
        this.currentAd = {}
      }
    }
  },

  _isInAd: function(currentAd) {
    var pos = this.player.media.currentTime
    return pos >= (currentAd.startTime - 1) && pos < (currentAd.endTime - 0.5)
  }
})

module.exports = youbora.adapters.MediatailorHls
